<?php

namespace App\Composers;

use App\Models\UserType;
use Illuminate\View\View;

class UserSave {

    protected $types;

    /**
     * Create a new profile composer.
     *
     * @param  UserType $types
     * @return void
     */
    public function __construct(UserType $types) {
        $this->types = $types->all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view) {
        $view->with('types', $this->types);
    }
}