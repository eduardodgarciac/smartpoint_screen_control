<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ScreenOrientation implements ShouldBroadcastNow {

    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $screens     = [];
    public  $orientation = [];

    /**
     * Create a new event instance.
     *
     * @param $screens
     * @param $orientation
     */
    public function __construct($screens, $orientation) {
        $this->screens     = $screens;
        $this->orientation = abs((int) $orientation) <= 3 ? abs((int) $orientation) : 0;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return $this->screens->map(function ($screen) {
            return new PrivateChannel("screen.{$screen->id}");
        })->toArray();
    }
}
