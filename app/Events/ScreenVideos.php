<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ScreenVideos implements ShouldBroadcastNow {

    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $screens = [];
    public  $videos  = [];

    /**
     * Create a new event instance.
     *
     * @param $screens
     * @param $videos
     */
    public function __construct($screens, $videos) {
        $this->screens = $screens;
        $this->videos  = array_pluck($videos, 'file');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return $this->screens->map(function ($screen) {
            return new PrivateChannel("screen.{$screen->id}");
        })->toArray();
    }
}
