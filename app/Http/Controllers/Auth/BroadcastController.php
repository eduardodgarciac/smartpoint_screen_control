<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller {

    public function __construct(Request $request) {
        if ($request->header('X-CSRF-TOKEN')) {
            $this->middleware(['web']);
        } else {
            $this->middleware(['api', 'auth:api']);
        }
    }

    public function __invoke(Request $request) {
        return Broadcast::auth($request);
    }
}
