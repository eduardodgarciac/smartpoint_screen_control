<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileSaveRequest;
use Illuminate\Http\Request;

class ProfileController extends Controller {

    public function index(Request $request) {
        if (!$request->expectsJson()) {
            return view('users.profile');
        }

        $user = auth()->user();

        return response()->json(compact('user'));
    }

    public function update(ProfileSaveRequest $request) {
        $user = auth()->user();
        $user->update($request->only('name', 'email', 'password'));

        return response()->json(compact('user'));
    }
}
