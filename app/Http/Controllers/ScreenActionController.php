<?php

namespace App\Http\Controllers;

use App\Events\ScreenOrientation;
use App\Events\ScreenReboot;
use App\Events\ScreenVideos;
use App\Models\Screen;
use Illuminate\Http\Request;

class ScreenActionController extends Controller {

    private $screens;

    public function __invoke($action, Request $request) {
        $this->authorize('action', Screen::class);

        $this->screens = Screen::whereIn('id', $request->input('ids', []))->where('user_id', auth()->user()->id)->get();

        return $this->{$action}($request);
    }

    private function videos(Request $request) {
        $videos = collect($request->input('videos'))->mapWithKeys(function ($video) {
            return [$video['id'] => ['index' => $video['index']]];
        })->all();
        $this->screens->each(function ($screen) use ($videos) {
            $screen->videos()->sync($videos);
        });

        event(new ScreenVideos($this->screens, $request->input('videos', [])));
        $message = 'Las pantallas selecciondas sincronizarán sus videos';

        return response()->json(compact('message'));
    }

    private function orientation(Request $request) {
        event(new ScreenOrientation($this->screens, $request->input('orientation', 0)));
        $message = 'Las pantallas selecciondas cambiarán su orientación';

        return response()->json(compact('message'));
    }

    private function reboot(Request $request) {
        event(new ScreenReboot($this->screens));
        $message = 'Las pantallas selecciondas se reiniciarán';

        return response()->json(compact('message'));
    }
}
