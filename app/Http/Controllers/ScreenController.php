<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScreenSaveRequest;
use App\Models\Screen;
use Illuminate\Http\Request;

class ScreenController extends Controller {

    public function __invoke() {
        return redirect()->route('screens.index');
    }

    public function index(Request $request) {
        if (!$request->expectsJson()) {
            return view('screens.index');
        }

        $screens = Screen::with('videos')->currentUser()->filter($request->input('search'))->paginate($request->input('perPage', 25));

        return response()->json(compact('screens'));
    }

    public function store(ScreenSaveRequest $request) {
        if ($request->user()->isClient()) {
            $screen = Screen::where('serial', $request->input('serial'))->firstOrFail();
            $screen->user()->associate($request->user())->fill($request->only('name'))->save();
        } else {
            $screen = Screen::create($request->all())->refresh();
        }

        return response()->json(compact('screen'));
    }

    public function update(ScreenSaveRequest $request, Screen $screen) {
        $this->authorize('update', $screen);

        if ($request->user()->isClient()) {
            $screen->update($request->only('name'));
        } else {
            $screen->update($request->only('mac'));
        }

        return response()->json(compact('screen'));
    }

    public function destroy(Screen $screen, Request $request) {
        $this->authorize('destroy', $screen);

        if ($request->user()->isClient()) {
            $screen->update(['user_id' => null, 'name' => null]);
        } else {
            $screen->delete();
        }

        return response()->json(compact('screen'));
    }
}
