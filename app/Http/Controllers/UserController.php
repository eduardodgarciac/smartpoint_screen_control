<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSaveRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function index(Request $request) {
        $this->authorize('index', User::class);

        if (!$request->expectsJson()) {
            return view('users.index');
        }

        $users = User::with('type')->filter($request->input('search'))->paginate($request->input('perPage', 25));

        return response()->json(compact('users'));
    }

    public function store(UserSaveRequest $request) {
        $this->authorize('store', User::class);

        $user = User::create($request->all())->refresh();
        $user->load('type');

        return response()->json(compact('user'));
    }

    public function update(UserSaveRequest $request, User $user) {
        $this->authorize('update', $user);

        $user->update($request->all());
        $user->load('type');

        return response()->json(compact('user'));
    }

    public function destroy(User $user) {
        $this->authorize('destroy', $user);

        $user->delete();

        return response()->json(compact('user'));
    }
}
