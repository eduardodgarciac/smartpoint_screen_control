<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideoSaveRequest;
use App\Models\Video;
use App\Support\VideoStream;
use Illuminate\Http\Request;

class VideoController extends Controller {

    public function index(Request $request) {
        $this->authorize('index', Video::class);

        if ($request->acceptsHtml()) {
            return view('videos.index');
        }

        $videos = $request->user()->videos()->get();

        return response()->json(compact('videos'));
    }

    public function store(VideoSaveRequest $request) {
        $this->authorize('store', Video::class);

        $video = $request->user()->videos()->create($request->all())->refresh();
        $video->createFile($request->file('video'));

        return response()->json(compact('video'));
    }

    public function update(VideoSaveRequest $request, Video $video) {
        $this->authorize('update', $video);

        $video->update($request->only('name'));

        return response()->json(compact('video'));
    }

    public function destroy(Video $video) {
        $this->authorize('destroy', $video);

        $video->deleteFile();
        $video->delete();

        return response()->json(compact('video'));
    }

    public function stream(Video $video) {
        $this->authorize('stream', $video);

        return response()->stream(function () use ($video) {
            with(new VideoStream($video->getFile()))->start();
        });
    }

    public function download(Video $video) {
        $this->authorize('download', $video);

        return response()->download($video->getFile(), "{$video->name}.{$video->extension}");
    }

}
