<?php

namespace App\Http\Requests;

use App\Rules\CurrentPassword;
use Illuminate\Foundation\Http\FormRequest;

class ProfileSaveRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'             => ['required', 'string', 'min:3', 'max:255'],
            'email'            => ['required', 'email', 'max:255', "unique:users,email,$this->id,id"],
            'current_password' => ['required', new CurrentPassword],
            'password'         => ['sometimes', 'string', 'min:6', 'confirmed'],
        ];
    }
}
