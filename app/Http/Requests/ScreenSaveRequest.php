<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ScreenSaveRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [];

        if ($this->user()->isAdmin()) {
            $rules = [
                'serial' => ['required', 'numeric', "unique:screens,serial,{$this->id},id"],
                'mac'    => ['required', 'string', 'regex:/^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$/', "unique:screens,mac,{$this->id},id"]
            ];
        } elseif ($this->user()->isClient()) {
            $rules = [
                'serial' => [
                    'required',
                    'numeric',
                    Rule::exists('screens')->where(function ($query) {
                        $query->whereNull('user_id')->orWhere('user_id', $this->user()->id);
                    })
                ],
                'name'   => ['required', 'min:3', 'max:255']
            ];
        }

        return $rules;
    }
}
