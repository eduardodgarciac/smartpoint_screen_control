<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

class VideoSaveRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'video' => ['sometimes', 'required', 'mimetypes:video/*', 'max:50000'],
            'name'  => ['sometimes', 'required', 'string', 'min:3', 'max:255']
        ];
    }

    /**
     * Get all of the input and files for the request.
     *
     * @param  array|mixed $keys
     * @return array
     */
    public function all($keys = null) {
        $file = $this->file('video');
        if ($file instanceof UploadedFile && $file->isValid()) {
            $this->merge([
                'name'      => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
                'extension' => $file->getClientOriginalExtension(),
                'weight'    => round($file->getClientSize() / 1000000, 2),
            ]);
        }

        return parent::all($keys);
    }

}
