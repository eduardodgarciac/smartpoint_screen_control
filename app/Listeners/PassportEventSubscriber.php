<?php

namespace App\Listeners;

class PassportEventSubscriber {

    /**
     * Handle access token creation events.
     */
    public function onAccessTokenCreated($event) {
        \DB::table('oauth_access_tokens')->where('id', '<>', $event->tokenId)->where('user_id', $event->userId)->where('client_id', $event->clientId)->delete();//->update(['revoked' => true]);
    }

    /**
     * Handle refresh token creation events.
     */
    public function onRefreshTokenCreated($event) {
        \DB::table('oauth_refresh_tokens')->where('id', '<>', $event->refreshTokenId)->where('access_token_id', '<>', $event->accessTokenId)->delete();//->update(['revoked' => true]);
    }

    /**
     * Register the listeners for the subscriber.
     */
    public function subscribe($events) {
        $events->listen('Laravel\Passport\Events\AccessTokenCreated', 'App\Listeners\PassportEventSubscriber@onAccessTokenCreated');
        $events->listen('Laravel\Passport\Events\RefreshTokenCreated', 'App\Listeners\PassportEventSubscriber@onRefreshTokenCreated');
    }

}