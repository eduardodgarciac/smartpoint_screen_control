<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Screen extends Authenticatable {

    use HasApiTokens;

    /*------------------------------------------------------------------------------
    | Attributes
    '------------------------------------------------------------------------------*/
    protected $table        = 'screens';
    protected $primaryKey   = 'serial';
    public    $incrementing = false;

    protected $fillable = ['user_id', 'name', 'serial', 'mac', 'password'];
    protected $hidden   = ['password', 'remember_token'];

    /*------------------------------------------------------------------------------
    | Relations
    '------------------------------------------------------------------------------*/
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function videos() {
        return $this->belongsToMany(Video::class, 'screens_videos', 'screen_serial', 'video_id');
    }

    /*------------------------------------------------------------------------------
    | Scopes
    '------------------------------------------------------------------------------*/
    public function scopeCurrentUser($query) {
        return $query->when(auth()->user()->isClient(), function ($when) {
            $when->where('user_id', auth()->user()->id);
        });
    }

    public function scopeFilter($query, $search = null) {
        return $query->when(!blank($search), function ($when) use ($search) {
            $when->where('serial', 'like', "%{$search}%")->orWhere('mac', 'like', "%{$search}%")->orWhere('name', 'like', "%{$search}%");
        });
    }

    /*------------------------------------------------------------------------------
    | Methods
    '------------------------------------------------------------------------------*/
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->password = $model->mac;
        });
    }

    public function isAdmin() {
        return (int) $this->user->isAdmin();
    }

    public function isClient() {
        return (int) $this->user->isClient();
    }

    public function isOwnerOf($id) {
        return (int) $this->user_id === (int) $id;
    }

    public function canJoinRoom($id) {
        return (int) $this->user_id === (int) $id;
    }

    public function findForPassport($mac) {
        return $this->where('mac', $mac)->first();
    }

    /*------------------------------------------------------------------------------
    | Accessors && Mutators
    '------------------------------------------------------------------------------*/
    public function getNameAttribute($value) {
        return blank($value) ? 'No identificado' : $value;
    }

    public function setMacAttribute($value) {
        $this->attributes['mac'] = strtoupper($value);
    }

    public function setPasswordAttribute($value) {
        if (!blank($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
}
