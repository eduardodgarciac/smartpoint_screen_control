<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use Notifiable;

    /*------------------------------------------------------------------------------
    | Attributes
    '------------------------------------------------------------------------------*/
    protected $table    = 'users';
    protected $fillable = ['type_id', 'name', 'email', 'password'];
    protected $hidden   = ['password', 'remember_token'];

    /*------------------------------------------------------------------------------
    | Relations
    '------------------------------------------------------------------------------*/
    public function type() {
        return $this->belongsTo(UserType::class, 'type_id');
    }

    public function screens() {
        return $this->hasMany(Screen::class, 'user_id');
    }

    public function videos() {
        return $this->hasMany(Video::class, 'user_id');
    }

    /*------------------------------------------------------------------------------
    | Scopes
    '------------------------------------------------------------------------------*/
    public function scopeFilter($query, $search = null) {
        return $query->when(!blank($search), function ($when) use ($search) {
            $when->where('email', 'like', "%{$search}%")->orWhere('name', 'like', "%{$search}%");
        });
    }

    /*------------------------------------------------------------------------------
    | Methos
    '------------------------------------------------------------------------------*/
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            if (blank($model->password)) {
                $model->password = str_random(8);
            }
        });
    }

    public function isAdmin() {
        return (int) $this->type_id === 1;
    }

    public function isClient() {
        return (int) $this->type_id === 2;
    }

    public function isOwnerOf($id) {
        return (int) $this->id === (int) $id;
    }

    public function canJoinRoom($id) {
        return (int) $this->id === (int) $id;
    }

    /*------------------------------------------------------------------------------
    | Accessors && Mutators
    '------------------------------------------------------------------------------*/
    public function setPasswordAttribute($value) {
        if (!blank($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
}
