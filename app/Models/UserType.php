<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model {

    /*------------------------------------------------------------------------------
    | Attributes
    '------------------------------------------------------------------------------*/
    protected $table    = 'users_types';
    protected $fillable = ['name'];

    /*------------------------------------------------------------------------------
    | Relations
    '------------------------------------------------------------------------------*/
    public function users() {
        return $this->hasMany(User::class, 'type_id');
    }
}
