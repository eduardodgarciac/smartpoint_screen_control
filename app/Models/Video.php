<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Video extends Model {

    /*------------------------------------------------------------------------------
    | Attributes
    '------------------------------------------------------------------------------*/
    protected $table    = 'videos';
    protected $fillable = ['name', 'extension', 'weight'];
    protected $appends  = ['file'];

    /*------------------------------------------------------------------------------
    | Relations
    '------------------------------------------------------------------------------*/
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function screens() {
        return $this->belongsToMany(Screen::class, 'screens_videos', 'video_id', 'screen_id');
    }

    /*------------------------------------------------------------------------------
    | Methods
    '------------------------------------------------------------------------------*/
    public function getFile() {
        return Storage::disk('videos')->getDriver()->getAdapter()->applyPathPrefix("{$this->user_id}/{$this->file}");
    }

    public function createFile($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile && $file->isValid()) {
            $file->storeAs($this->user_id, $this->file, 'videos');
        }
    }

    public function deleteFile() {
        Storage::disk('videos')->delete("{$this->user_id}/{$this->file}");
        if (!Storage::disk('videos')->allFiles($this->user_id)) {
            Storage::disk('videos')->deleteDirectory($this->user_id);
        }
    }

    /*------------------------------------------------------------------------------
    | Accessors && Mutators
    '------------------------------------------------------------------------------*/
    public function getFileAttribute() {
        return "{$this->getAttribute('id')}.{$this->getAttribute('extension')}";
    }

}
