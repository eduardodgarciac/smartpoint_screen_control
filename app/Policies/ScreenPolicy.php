<?php

namespace App\Policies;

use App\Models\Screen;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ScreenPolicy {

    use HandlesAuthorization;

    public function update(User $user, Screen $screen) {
        return $user->isAdmin() || $user->isOwnerOf($screen->user_id);
    }

    public function destroy(User $user, Screen $screen) {
        return $user->isAdmin() || $user->isOwnerOf($screen->user_id);
    }

    public function action(User $user) {
        return $user->isClient();
    }
}
