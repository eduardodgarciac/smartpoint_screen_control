<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Video;
use Illuminate\Auth\Access\HandlesAuthorization;

class VideoPolicy {

    use HandlesAuthorization;

    public function index(User $user) {
        return $user->isClient();
    }

    public function store(User $user) {
        return $user->isClient();
    }

    public function update(User $user, Video $video) {
        return $user->isClient() && $user->isOwnerOf($video->user_id);
    }

    public function destroy(User $user, Video $video) {
        return $user->isClient() && $user->isOwnerOf($video->user_id);
    }

    public function stream(User $user, Video $video) {
        return $user->isClient() && $user->isOwnerOf($video->user_id);
    }

    public function download($user, Video $video) {
        return $user->isClient() && $user->isOwnerOf($video->user_id);
    }
}
