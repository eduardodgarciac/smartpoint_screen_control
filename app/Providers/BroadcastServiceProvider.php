<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Route::post('broadcasting/auth', '\App\Http\Controllers\Auth\BroadcastController');

        require base_path('routes/channels.php');
    }
}
