<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScreensTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('screens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('serial')->unsigned()->unique();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->macAddress('mac')->unique();
            $table->string('password');
            //$table->enum('orientation', ['Arriba', 'Abajo', 'Izquierda', 'Derecha'])->default('Arriba');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('screens');
    }
}
