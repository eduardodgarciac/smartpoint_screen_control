<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScreensVideosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('screens_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('screen_serial')->unsigned();
            $table->integer('video_id')->unsigned();
            $table->tinyInteger('index')->unsiged();
            $table->timestamps();

            $table->foreign('screen_serial')->references('serial')->on('screens');
            $table->foreign('video_id')->references('id')->on('videos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('screens_videos');
    }
}
