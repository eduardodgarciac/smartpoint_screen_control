<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        foreach (['Administrador', 'Cliente'] as $type) {
            \App\Models\UserType::create(['name' => $type]);
        }
        \App\Models\User::create(['type_id' => 1, 'name' => 'Jaime Ahumada', 'email' => 'jahumada@fastersoluciones.cl', 'password' => 'fastersoluciones2018']);
        \App\Models\User::create(['type_id' => 1, 'name' => 'Pedro Rivas', 'email' => 'pedro@fastersoluciones.cl', 'password' => 'fastersoluciones2018']);
        \App\Models\User::create(['type_id' => 1, 'name' => 'Eduardo García', 'email' => 'eduardodgarciac@gmail.com', 'password' => 'fastersoluciones2018']);
        \App\Models\User::create(['type_id' => 2, 'name' => 'Cliente', 'email' => 'cliente@example.com', 'password' => 'fastersoluciones2018']);

        \DB::table('oauth_clients')->insert(['name' => 'Password Grant Client', 'secret' => 'fQGapsQR3V1rcQP209NSyWOG36WjDlSsIdw1rmFn', 'redirect' => 'http://localhost', 'personal_access_client' => 0, 'password_client' => 1, 'revoked' => 0, 'created_at' => now(), 'updated_at' => now()]);
        //$this->call(UsersTableSeeder::class);
    }
}
