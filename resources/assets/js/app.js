require('./bootstrap');

Vue.prototype.$setErrors = function (data, errors) {
    _.each(_.get(data, 'response.data.errors', []), (value, key) => {
        errors.add(key, value[0], 'server');
    });
};

Vue.prototype.$toastError = function (msg) {
    this.$toast.open({message: msg, position: 'is-bottom', type: 'is-danger'});
};

Vue.prototype.$toastSuccess = function (msg) {
    this.$toast.open({message: msg, position: 'is-bottom', type: 'is-success'});
};

Vue.directive('cleave', {
    bind(el, binding) {
        const input = el.querySelector('input');
        input._vCleave = new Cleave(input, binding.value);
    },
    update(el, binding) {
        const input = el.querySelector('input');
        input._vCleave.destroy();
        input._vCleave = new Cleave(input, binding.value);
    },
    unbind(el) {
        const input = el.querySelector('input');
        input._vCleave.destroy();
    }
});

const app = new Vue({
    el: '#app',
    components: {
        'screens-index': require('./components/screens/Index.vue'),
        'videos-index': require('./components/videos/Index.vue'),
        'users-index': require('./components/users/Index.vue'),
        'users-profile': require('./components/users/Profile.vue')
    },
    data: {
        isActive: false
    }
});