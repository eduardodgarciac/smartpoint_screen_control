window._ = require('lodash');
window.Cleave = require('cleave.js');

window.axios = require('axios');
window.axios.defaults.headers.common = {
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

window.io = require('socket.io-client');
window.echo = new (require('laravel-echo'))({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

window.Vue = require('vue');
window.Buefy = require('buefy');
window.VeeValidate = require('vee-validate');

Vue.use(Buefy.default);
Vue.use(VeeValidate);