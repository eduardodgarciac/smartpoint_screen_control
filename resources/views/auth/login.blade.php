@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="{{ route('login') }}">
                    <p class="title is-5">Iniciar sesión</p>

                    {{ csrf_field() }}

                    <b-field label="Correo">
                        <b-input type="email" name="email" expanded></b-input>
                    </b-field>

                    <b-field label="Clave">
                        <b-input type="password" name="password" expanded></b-input>
                    </b-field>

                    <b-field>
                        <b-checkbox name="remember">Recuérdame</b-checkbox>
                    </b-field>

                    <b-field grouped position="is-right">
                        <div class="control">
                            <a class="button" href="{{ route('password.request') }}">Olvidé mi contraseña</a>
                        </div>
                        <div class="control">
                            <button type="submit" class="button is-primary">Entrar</button>
                        </div>
                    </b-field>
                </form>
            </div>
        </div>
    </div>
@endsection
