@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="{{ route('password.email') }}">

                    <p class="title is-5">Resetear contraseña</p>

                    {{ csrf_field() }}

                    <b-field horizontal label="Correo">
                        <b-input type="email" name="email" expanded></b-input>
                    </b-field>

                    <b-field grouped position="is-right">
                        <div class="control">
                            <a class="button" href="{{ route('login') }}">Cancelar</a>
                        </div>
                        <div class="control">
                            <button type="submit" class="button is-primary">Enviar</button>
                        </div>
                    </b-field>
                </form>
            </div>
        </div>
    </div>
@endsection
