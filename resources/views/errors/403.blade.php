@extends('errors::layout')

@section('title', trans('auth.unauthorized'))

@section('message', trans('auth.unauthorized'))
