@extends('errors::layout')

@section('title', 'Página no encontrada')

@section('message', 'Lo siento, la página que estas buscando no pudo ser encontrada')
