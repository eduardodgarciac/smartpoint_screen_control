<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ config('app.name') }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    </head>
    <body>
        <div id="app" v-cloak>
            @include('partials.navbar')
            <div id="content">
                @yield('content')
            </div>
        </div>
        <script src="{{ asset(mix('js/app.js')) }}"></script>
    </body>
</html>
