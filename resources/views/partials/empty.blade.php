<template slot="empty">
    <section class="section">
        <div class="content has-text-grey has-text-centered">
            <p>
                <b-icon icon="emoticon-sad" size="is-large"></b-icon>
            </p>
            <p>{{ $slot }}</p>
        </div>
    </section>
</template>