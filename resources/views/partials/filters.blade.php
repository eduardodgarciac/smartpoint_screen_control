<div class="columns is-mobile is-multiline">
    @if($pagination ?? true)
        <div class="column is-12-mobile is-narrow">
            <b-field horizontal label="Mostrar:">
                <b-select v-model="query.perPage" @input="all()">
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="250">250</option>
                </b-select>
            </b-field>
        </div>
    @endif
    <div class="column is-12-mobile">
        <b-field expanded>
            <b-input type="text" placeholder="Buscar..." icon="magnify" v-model="query.search" @keyup.native.enter="all()" expanded></b-input>
        </b-field>
    </div>
</div>