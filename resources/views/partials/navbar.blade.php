<nav class="navbar is-transparent">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ route('screens.index') }}">
            {{ config('app.name') }}
        </a>
        @auth
            <button type="button" class="navbar-burger button is-white" :class="{ 'is-active': isActive }" @click="isActive = !isActive">
                <span></span>
                <span></span>
                <span></span>
            </button>
        @endauth
    </div>

    @auth
        <div class="navbar-menu" :class="{ 'is-active': isActive }">
            <div class="navbar-end">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">{{ auth()->user()->name }}</a>
                    <div class="navbar-dropdown is-boxed is-right">
                        <a class="navbar-item @active('screens.index')" href="{{ route('screens.index') }}">Pantallas</a>
                        @if(auth()->user()->isClient())
                            <a class="navbar-item @active('videos.index')" href="{{ route('videos.index') }}">Videos</a>
                        @endif
                        @if(auth()->user()->isAdmin())
                            <a class="navbar-item @active('users.index')" href="{{ route('users.index') }}">Usuarios</a>
                        @endif
                        <a class="navbar-item @active('profile.index')" href="{{ route('profile.index') }}">Perfil</a>
                        <hr class="navbar-divider">
                        <a class="navbar-item" href="{{ route('logout') }}">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    @endauth
</nav>