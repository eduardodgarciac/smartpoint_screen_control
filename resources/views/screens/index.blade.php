@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column">
            <screens-index inline-template :user="{{ auth()->user() }}">
                <div class="box">
                    <nav class="level is-mobile">
                        <div class="level-left">
                            <div class="level-item">
                                <p class="title is-5">Pantallas</p>
                            </div>
                        </div>

                        <div class="level-right">
                            <div class="level-item">
                                <a @click="openSaveModal({})">
                                    <b-icon icon="plus-circle" type="is-primary"></b-icon>
                                </a>
                            </div>
                            <div class="level-item">
                                <a @click="all">
                                    <b-icon icon="refresh" type="is-info"></b-icon>
                                </a>
                            </div>
                        </div>
                    </nav>

                    @include('screens.menu')

                    @include('partials.filters')

                    <b-table
                            :data="screens"
                            :loading="isLoading"

                            paginated
                            backend-pagination
                            :total="query.total"
                            :per-page="query.perPage"
                            @page-change="all()"

                            :checkable="isClient()"
                            :checked-rows.sync="selectedScreens">
                        <template slot-scope="props">
                            <b-table-column label="S/N" centered width="75">
                                @{{ props.row.serial }}
                            </b-table-column>
                            <b-table-column label="MAC" centered width="75" v-if="!isClient()">
                                @{{ props.row.mac }}
                            </b-table-column>
                            <b-table-column label="Nombre">
                                @{{ props.row.name }}
                            </b-table-column>
                            <b-table-column label="Estatus" centered width="40" v-if="isClient()">
                                <b-icon icon="circle" :type="props.row.status ? 'is-success' : 'is-danger'"></b-icon>
                            </b-table-column>
                            <b-table-column label="" width="40">
                                <b-field>
                                    <p class="control">
                                        <button type="button" class="button is-small" @click="openSaveModal(props.row)">
                                            <b-icon icon="pencil" type="is-info"></b-icon>
                                        </button>
                                    </p>
                                    <p class="control">
                                        <button type="button" class="button is-small" @click="remove(props.row)">
                                            <b-icon icon="delete" type="is-danger"></b-icon>
                                        </button>
                                    </p>
                                </b-field>
                            </b-table-column>
                        </template>

                        @component('partials.empty')
                            No se han encontrado pantallas.
                        @endcomponent
                    </b-table>

                    @include('screens.save')
                </div>
            </screens-index>
        </div>
    </div>
@endsection
