<screens-menu inline-template :selection.sync="selectedScreens" class="screens-menu" v-if="isClient()">
    <div class="columns is-mobile is-multiline is-centered">
        <div class="column is-narrow is-12-mobile has-text-centered-mobile has-text-right-tablet">
            <b-dropdown class="menu-item" :disabled="!selection.length">
                <button type="button" class="button" slot="trigger">
                    <span>Videos</span>
                    <b-icon icon="menu-down"></b-icon>
                </button>
                <b-dropdown-item custom paddingless>
                    <div class="modal-card is-marginless is-fullwidth" style="max-width: 100%; max-height: 300px;">
                        <draggable class="modal-card-body is-fullwidth" v-model="videos" :options="{handle: 'a.control'}">
                            <div class="field is-inline-flex" style="width: 100%; align-items: center;" v-for="(video, index) in videos" :key="video.id">
                                <div class="control" style="flex: 1;">
                                    <b-checkbox v-model="video.selected">@{{ index + 1 }}. @{{ video.name }}</b-checkbox>
                                </div>
                                <a class="control">
                                    <b-icon icon="drag-vertical"></b-icon>
                                </a>
                            </div>
                        </draggable>
                        <footer class="modal-card-foot">
                            <button type="button" class="button is-primary is-marginless" @click="setVideos">Guardar
                            </button>
                        </footer>
                    </div>
                </b-dropdown-item>
            </b-dropdown>
        </div>
        <div class="column is-narrow is-12-mobile has-text-centered-mobile has-text-centered-tablet">
            <b-dropdown class="menu-item" :disabled="!selection.length">
                <button type="button" class="button" slot="trigger">
                    <span>Orientación</span>
                    <b-icon icon="menu-down"></b-icon>
                </button>
                <b-dropdown-item @click="setOrientation(0)">Arriba</b-dropdown-item>
                <b-dropdown-item @click="setOrientation(1)">Derecha</b-dropdown-item>
                <b-dropdown-item @click="setOrientation(2)">Abajo</b-dropdown-item>
                <b-dropdown-item @click="setOrientation(3)">Izquierda</b-dropdown-item>
            </b-dropdown>
        </div>
        <div class="column is-narrow is-12-mobile has-text-centered-mobile has-text-left-tablet">
            <button type="button" class="button menu-item" :disabled="!selection.length" @click="reboot">Reiniciar</button>
        </div>
    </div>
</screens-menu>