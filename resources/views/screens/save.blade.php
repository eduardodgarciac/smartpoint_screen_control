<b-modal :active.sync="showSaveModal" :on-cancel="closeSaveModal" has-modal-card>
    <screens-save inline-template :model="screen" @add="add" @edit="edit">
        <form>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">@{{ screen.id ? 'Editar' : 'Agregar' }} pantalla</p>
                </header>
                <section class="modal-card-body">
                    <b-field label="Serial" :type="errors.has('serial') ? 'is-danger': ''" :message="errors.first('serial')">
                        <b-input type="text" name="serial" v-model="screen.serial" v-validate :readonly="screen.id"></b-input>
                    </b-field>
                    @if(auth()->user()->isAdmin())
                        <b-field label="MAC" :type="errors.has('mac') ? 'is-danger': ''" :message="errors.first('mac')">
                            <b-input type="text" name="mac" v-model="screen.mac" v-validate v-cleave="macMask"></b-input>
                        </b-field>
                    @else
                        <b-field label="Nombre" :type="errors.has('name') ? 'is-danger': ''" :message="errors.first('name')">
                            <b-input type="text" name="name" v-model="screen.name" v-validate autofocus></b-input>
                        </b-field>
                    @endif
                </section>
                <footer class="modal-card-foot">
                    <button type="reset" class="button" @click="close">Cancelar</button>
                    <button type="button" class="button is-primary" @click="save" :disabled="errors.any()">Guardar</button>
                </footer>
            </div>
        </form>
    </screens-save>
</b-modal>