@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column">
            <users-index inline-template>
                <div class="box">
                    <nav class="level is-mobile">
                        <div class="level-left">
                            <div class="level-item">
                                <p class="title is-5">Usuarios</p>
                            </div>
                        </div>

                        <div class="level-right">
                            <div class="level-item">
                                <a @click="openSaveModal({})">
                                    <b-icon icon="plus-circle" type="is-primary"></b-icon>
                                </a>
                            </div>
                            <div class="level-item">
                                <a @click="all">
                                    <b-icon icon="refresh" type="is-info"></b-icon>
                                </a>
                            </div>
                        </div>
                    </nav>

                    @include('partials.filters')

                    <b-table
                            :data="users"
                            :loading="isLoading"

                            paginated
                            backend-pagination
                            :total="query.total"
                            :per-page="query.perPage"
                            @page-change="all()"
                    >
                        <template slot-scope="props">
                            <b-table-column label="Tipo" width="150">
                                @{{ props.row.type.name }}
                            </b-table-column>
                            <b-table-column label="Nombre" width="200">
                                @{{ props.row.name }}
                            </b-table-column>
                            <b-table-column label="Email">
                                @{{ props.row.email }}
                            </b-table-column>
                            <b-table-column label="" width="40">
                                <b-field>
                                    <p class="control">
                                        <button type="button" class="button is-small" @click="openSaveModal(props.row)">
                                            <b-icon icon="pencil" type="is-info"></b-icon>
                                        </button>
                                    </p>
                                    <p class="control">
                                        <button type="button" class="button is-small" @click="remove(props.row)">
                                            <b-icon icon="delete" type="is-danger"></b-icon>
                                        </button>
                                    </p>
                                </b-field>
                            </b-table-column>
                        </template>

                        @component('partials.empty')
                            No se han encontrado usuarios.
                        @endcomponent
                    </b-table>

                    @include('users.save')
                </div>
            </users-index>
        </div>
    </div>
@endsection
