@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column is-8">
            <users-profile inline-template>
                <div class="box">
                    <form>
                        <p class="title is-5">Perfil</p>

                        <b-field label="Nombre*" :type="errors.has('name') ? 'is-danger': ''" :message="errors.first('name')">
                            <b-input type="text" name="name" v-model="user.name" v-validate expanded></b-input>
                        </b-field>

                        <b-field label="Correo*" :type="errors.has('email') ? 'is-danger': ''" :message="errors.first('email')">
                            <b-input type="email" name="email" v-model="user.email" v-validate expanded></b-input>
                        </b-field>

                        <b-field label="Clave actual*" :type="errors.has('current_password') ? 'is-danger': ''" :message="errors.first('current_password')">
                            <b-input type="password" name="current_password" v-model="user.current_password" v-validate expanded></b-input>
                        </b-field>

                        <b-field label="Clave nueva" :type="errors.has('password') ? 'is-danger': ''" :message="errors.first('password')">
                            <b-input type="password" name="password" v-model="user.password" v-validate expanded></b-input>
                        </b-field>

                        <b-field label="Confirmar clave">
                            <b-input type="password" name="password_confirmation" v-model="user.password_confirmation" expanded></b-input>
                        </b-field>

                        <b-field grouped position="is-right">
                            <div class="control">
                                <button type="button" class="button is-primary" @click="save">Guardar</button>
                            </div>
                        </b-field>
                    </form>
                </div>
            </users-profile>
        </div>
    </div>
@endsection
