<b-modal :active.sync="showSaveModal" :on-cancel="closeSaveModal" has-modal-card>
    <users-save inline-template :model="user" @add="add" @edit="edit">
        <form>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">@{{ user.id ? 'Editar' : 'Agregar' }} usuario</p>
                </header>
                <section class="modal-card-body">
                    <b-field label="Tipo" :type="errors.has('type_id') ? 'is-danger': ''" :message="errors.first('type_id')">
                        <b-select name="type_id" v-model="user.type_id" v-validate expanded>
                            @foreach($types as $type)
                                <option :value="{{ $type->id }}" :key="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </b-select>
                    </b-field>
                    <b-field label="Nombre" :type="errors.has('name') ? 'is-danger': ''" :message="errors.first('name')">
                        <b-input type="text" name="name" v-model="user.name" v-validate expanded></b-input>
                    </b-field>
                    <b-field label="Correo" :type="errors.has('email') ? 'is-danger': ''" :message="errors.first('email')">
                        <b-input type="email" name="email" v-model="user.email" v-validate expanded></b-input>
                    </b-field>
                </section>
                <footer class="modal-card-foot">
                    <button type="reset" class="button" @click="close">Cancelar</button>
                    <button type="button" class="button is-primary" @click="save" :disabled="errors.any()">Guardar</button>
                </footer>
            </div>
        </form>
    </users-save>
</b-modal>