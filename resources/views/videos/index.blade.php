@extends('layouts.app')

@section('content')
    <div class="columns is-centered">
        <div class="column">
            <videos-index inline-template>
                <div class="box">
                    <nav class="level is-mobile">
                        <div class="level-left">
                            <div class="level-item">
                                <p class="title is-5">Videos</p>
                            </div>
                        </div>

                        <div class="level-right">
                            <div class="level-item">
                                <a @click="openSaveModal({})">
                                    <b-icon icon="upload" type="is-primary"></b-icon>
                                </a>
                            </div>
                            <div class="level-item">
                                <a @click="all">
                                    <b-icon icon="refresh" type="is-info"></b-icon>
                                </a>
                            </div>
                        </div>
                    </nav>

                    @include('partials.filters', ['pagination' => false])

                    <b-table :data="filteredVideos" :loading="isLoading">
                        <template slot-scope="props">
                            <b-table-column label="Nombre">
                                @{{ props.row.name }}
                            </b-table-column>
                            <b-table-column label="Ext" centered>
                                @{{ props.row.extension }}
                            </b-table-column>
                            <b-table-column label="Peso" numeric>
                                @{{ props.row.weight }} MB
                            </b-table-column>
                            <b-table-column label="" width="40">
                                <b-field>
                                    <p class="control">
                                        <a class="button is-small" target="_blank" :href="'{{ route('videos.index') }}/' + props.row.id + '/stream'">
                                            <b-icon icon="play" type="is-success"></b-icon>
                                        </a>
                                    </p>
                                    <p class="control">
                                        <a class="button is-small" target="_blank" :href="'{{ route('videos.index') }}/' + props.row.id + '/download'">
                                            <b-icon icon="download" type="is-link"></b-icon>
                                        </a>
                                    </p>
                                    <p class="control">
                                        <button type="button" class="button is-small" @click="openSaveModal(props.row)">
                                            <b-icon icon="pencil" type="is-info"></b-icon>
                                        </button>
                                    </p>
                                    <p class="control">
                                        <button class="button is-small" @click="remove(props.row)">
                                            <b-icon icon="delete" type="is-danger"></b-icon>
                                        </button>
                                    </p>
                                </b-field>
                            </b-table-column>
                        </template>

                        @component('partials.empty')
                            No se han encontrado videos.
                        @endcomponent
                    </b-table>

                    @include('videos.save')
                </div>
            </videos-index>
        </div>
    </div>
@endsection
