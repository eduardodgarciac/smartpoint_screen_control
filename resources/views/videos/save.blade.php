<b-modal :active.sync="showSaveModal" :on-cancel="closeSaveModal" has-modal-card>
    <videos-save inline-template :model="video" @add="add" @edit="edit">
        <form>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">@{{ video.id ? 'Editar video' : 'Cargar videos' }}</p>
                </header>

                <section class="modal-card-body" v-if="!video.id">
                    <b-field>
                        <b-upload v-model="videos" multiple drag-drop accept="video/*">
                            <section class="section">
                                <div class="content has-text-centered">
                                    <p>
                                        <b-icon icon="upload" size="is-large"></b-icon>
                                    </p>
                                    <p>Arrastre sus videos aquí o haga click para seleccionar</p>
                                </div>
                            </section>
                        </b-upload>
                    </b-field>

                    <progress class="progress is-info is-small" :value="progress" max="100">@{{ progress }}%</progress>

                    <div class="tags">
                        <span v-for="(video, index) in videos" :key="index" class="tag is-primary">
                            @{{ video.name }}
                            <button type="button" class="delete is-small" @click="remove(index)"></button>
                        </span>
                    </div>
                </section>


                <section class="modal-card-body" v-if="video.id">
                    <b-field label="Nombre" :type="errors.has('name') ? 'is-danger': ''" :message="errors.first('name')">
                        <b-input type="text" name="name" v-model="video.name" v-validate expanded></b-input>
                    </b-field>
                </section>

                <footer class="modal-card-foot">
                    <button type="reset" class="button" @click="close">Cancelar</button>
                    <button type="button" class="button is-primary" @click="save" :disabled="errors.any()">@{{ video.id ? 'Guardar' : 'Cargar' }}</button>
                </footer>
            </div>
        </form>
    </videos-save>
</b-modal>