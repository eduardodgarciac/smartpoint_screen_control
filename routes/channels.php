<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('user.{id}.screens', function ($user, $id) {
    if ($user->canJoinRoom($id)) {
        return $user;
    }
});

Broadcast::channel('screen.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});