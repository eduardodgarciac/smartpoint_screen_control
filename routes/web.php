<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*------------------------------------------------------------------------------
| Auth && Passwords
'------------------------------------------------------------------------------*/
Route::get('login', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', '\App\Http\Controllers\Auth\LoginController@login');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::group(['prefix' => 'password'], function () {
    Route::post('email', '\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('reset', '\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset');
    Route::get('reset/{token}', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
});

/*------------------------------------------------------------------------------
| App
'------------------------------------------------------------------------------*/
Route::group(['middleware' => 'auth'], function () {
    Route::get('', 'ScreenController');
    Route::post('screens/{action}', 'ScreenActionController')->where('action', 'videos|orientation|reboot');
    Route::resource('screens', 'ScreenController', ['only' => ['index', 'store', 'update', 'destroy']]);

    Route::get('videos/{video}/stream', 'VideoController@stream');
    Route::get('videos/{video}/download', 'VideoController@download');
    Route::resource('videos', 'VideoController', ['only' => ['index', 'store', 'update', 'destroy']]);

    Route::resource('users/profile', 'ProfileController', ['only' => ['index', 'update']]);
    Route::resource('users', 'UserController', ['only' => ['index', 'store', 'update', 'destroy']]);
});